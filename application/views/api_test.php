<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Test Page</title>

    <style>
        .container {
            width: 302px;
            height: 175px;
            position: absolute;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 0px;
            margin: auto;
        }

        #number,
        #verificationcode {
            width: calc(100% - 24px);
            padding: 10px;
            font-size: 20px;
            margin-bottom: 5px;
            outline: none;
        }

        #recaptcha-container {
            margin-bottom: 5px;
        }

        #send,
        #verify {
            width: 100%;
            height: 40px;
            outline: none;
        }

        .p-conf,
        .n-conf {
            width: calc(100% - 22px);
            border: 2px solid green;
            border-radius: 4px;
            padding: 8px 10px;
            margin: 4px 0px;
            background-color: rgba(0, 249, 12, 0.5);
            display: none;
        }

        .n-conf {
            border-color: red;
            background-color: rgba(255, 0, 4, 0.5);
        }
    </style>

</head>

<body>

    <h1>API Test Page</h1>

    <?php echo form_open('api/sendVerificationCode', 'id="apiForm"'); ?>
    <!-- <div class="container">
        <div id="sender"> -->
    <input type="text" name="phoneNumber" id="phoneNumber" placeholder="+923...">
    <input type="hidden" name="recaptcha_token" id="recaptchaToken">
    <div id="recaptcha-container"></div>
    <button type="submit">Send</button>
    <!-- </div> -->
    <!-- </div> -->
    <?php echo form_close(); ?>

    <!--	add firebase SDK-->
    <script src="https://www.gstatic.com/firebasejs/9.12.1/firebase-app-compat.js"></script>
    <script src="https://www.gstatic.com/firebasejs/9.12.1/firebase-auth-compat.js"></script>
    <script>
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        const firebaseConfig = {
            apiKey: "AIzaSyBG3A_gy2jvO7QHCq-sErJ1BKcHkAFTTyI",
            authDomain: "investro-6d552.firebaseapp.com",
            databaseURL: "https://investro-6d552-default-rtdb.firebaseio.com",
            projectId: "investro-6d552",
            storageBucket: "investro-6d552.appspot.com",
            messagingSenderId: "908309953408",
            appId: "1:908309953408:web:18141971fccbccb7fd5f65",
            measurementId: "G-M7M4QPJCSV"
        };
        firebase.initializeApp(firebaseConfig);
        render();
        function render() {

            function onRecaptchaResolved(token) {
                document.getElementById('recaptchaToken').value = token;
                console.log("Recaptcha Token:", token);
            }
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
                'size': 'normal',
                'callback': onRecaptchaResolved,
                'expired-callback': function () {
                }
            }); recaptchaVerifier.render();
        }
        // function for send message
        function phoneAuth() {
            var number = document.getElementById('number').value;
            firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
                window.confirmationResult = confirmationResult;
                coderesult = confirmationResult;
                document.getElementById('sender').style.display = 'none';
                document.getElementById('verifier').style.display = 'block';
            }).catch(function (error) {
                alert(error.message);
            });
        }
        // function for code verify
        function codeverify() {
            var code = document.getElementById('verificationcode').value;
            coderesult.confirm(code).then(function () {
                document.getElementsByClassName('p-conf')[0].style.display = 'block';
                document.getElementsByClassName('n-conf')[0].style.display = 'none';
            }).catch(function () {
                document.getElementsByClassName('p-conf')[0].style.display = 'none';
                document.getElementsByClassName('n-conf')[0].style.display = 'block';
            })
        }
    </script>

</body>

</html>