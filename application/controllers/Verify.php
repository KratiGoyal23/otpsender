<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verify extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        // $this->load->helper('json');
    }
    public function index()
    {

        $verificationCode = $this->input->post('verificationCode');
        $sessionInfo = $this->input->post('sessionInfo');

        // Prepare data for the signInWithPhoneNumber POST request
        $verifyPhoneNumberData = [
            'sessionInfo' => $sessionInfo,
            'code' => $verificationCode,
        ];

        $apiKey = 'AIzaSyBG3A_gy2jvO7QHCq-sErJ1BKcHkAFTTyI';
        $endpoint = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPhoneNumber?key=' . $apiKey;

        // Initialize cURL session for signInWithPhoneNumber
        $chVerifyPhoneNumber = curl_init($endpoint);

        // Set cURL options for signInWithPhoneNumber
        curl_setopt($chVerifyPhoneNumber, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chVerifyPhoneNumber, CURLOPT_POST, true);
        curl_setopt($chVerifyPhoneNumber, CURLOPT_POSTFIELDS, json_encode($verifyPhoneNumberData));
        curl_setopt($chVerifyPhoneNumber, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);

        // Execute cURL session for signInWithPhoneNumber and get the response
        $responseVerifyPhoneNumber = curl_exec($chVerifyPhoneNumber);

        // Check for cURL errors for signInWithPhoneNumber
        if (curl_errno($chVerifyPhoneNumber)) {
            echo 'Curl error: ' . curl_error($chVerifyPhoneNumber);
        }

        // Get HTTP status code
        $httpStatusCode = curl_getinfo($chVerifyPhoneNumber, CURLINFO_HTTP_CODE);

        // Close cURL session for signInWithPhoneNumber
        curl_close($chVerifyPhoneNumber);

        // Check if verification was successful
        if ($httpStatusCode === 200) {
            echo 'Phone number verified successfully!';
        } else {
            echo 'Error verifying phone number. HTTP status code: ' . $httpStatusCode;
            // Handle OTP error here
        }

        // Decode and print the response for signInWithPhoneNumber
        $responseDataVerifyPhoneNumber = json_decode($responseVerifyPhoneNumber, true);
        print_r($responseDataVerifyPhoneNumber);
    }

}


?>