<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        // $this->load->helper('json');
    }

    public function index()
    {
        $this->load->view('api_test');
    }

    public function sendVerificationCode()
    {
        $phoneNumber = $this->input->post('phoneNumber');
        $recaptchaToken = $this->input->post('recaptcha_token');

        // Define the API key and endpoint
        $apiKey = 'AIzaSyBG3A_gy2jvO7QHCq-sErJ1BKcHkAFTTyI';
        $endpoint = 'https://identitytoolkit.googleapis.com/v1/accounts:sendVerificationCode?key=' . $apiKey;

        // Prepare the data for the POST request
        $data = [
            'phoneNumber' => $phoneNumber,
            'recaptchaToken' => $recaptchaToken
        ];

        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);

        // Decode and print the response
        $responseData = json_decode($response, true);
        print_r($responseData);

        $sessionInfo = $responseData['sessionInfo'];

        // HTML form for user input (you need to implement this in your UI)
        echo '
    <form method="post" action="codeverify">
        <label for="verificationCode">Enter Verification Code:</label>
        <input type="text" name="verificationCode" id="verificationCode" required>
        <input type="hidden" name="sessionInfo" value="' . $sessionInfo . '">
        <input type="submit" value="Verify">
    </form>';

    }

    public function codeverify()
    {

        $verificationCode = $this->input->post('verificationCode');
        $sessionInfo = $this->input->post('sessionInfo');

        // Prepare data for the signInWithPhoneNumber POST request
        $verifyPhoneNumberData = [
            'sessionInfo' => $sessionInfo,
            'code' => $verificationCode,
        ];

        $apiKey = 'AIzaSyBG3A_gy2jvO7QHCq-sErJ1BKcHkAFTTyI';
        $endpoint = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPhoneNumber?key=' . $apiKey;

        // Initialize cURL session for signInWithPhoneNumber
        $chVerifyPhoneNumber = curl_init($endpoint);

        // Set cURL options for signInWithPhoneNumber
        curl_setopt($chVerifyPhoneNumber, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chVerifyPhoneNumber, CURLOPT_POST, true);
        curl_setopt($chVerifyPhoneNumber, CURLOPT_POSTFIELDS, json_encode($verifyPhoneNumberData));
        curl_setopt($chVerifyPhoneNumber, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);

        // Execute cURL session for signInWithPhoneNumber and get the response
        $responseVerifyPhoneNumber = curl_exec($chVerifyPhoneNumber);

        // Check for cURL errors for signInWithPhoneNumber
        if (curl_errno($chVerifyPhoneNumber)) {
            echo 'Curl error: ' . curl_error($chVerifyPhoneNumber);
        }

        // Get HTTP status code
        $httpStatusCode = curl_getinfo($chVerifyPhoneNumber, CURLINFO_HTTP_CODE);

        // Close cURL session for signInWithPhoneNumber
        curl_close($chVerifyPhoneNumber);

        // Check if verification was successful
        if ($httpStatusCode === 200) 
        {
            echo 'Phone number verified successfully!';
        } 
        else {
            echo 'Error verifying phone number. HTTP status code: ' . $httpStatusCode;
        }
    }

}

?>